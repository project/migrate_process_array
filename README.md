# Migrate Process Array

When migrating content, sometimes you have an array of values you need to filter against known items. In PHP, you would use `array_intersect()` or  `array_diff()`. 

This module provides migration process plugins for each:

```yaml
  field_of_array_values:
    plugin: array_intersect
    source: some_array_field
    match:
      - values
      - to
      - match
  field_of_array_values:
    plugin: array_diff
    source: some_array_field
    exclude:
      - values
      - to
      - match
```

For a given field value:

```
[
  'a',
  'bunch',
  'of',
  'values',
];
```

The `array_intersect` plugin would return:

```
[
  'values',
];
```

Whereas the `array_diff` plugin would return:

```
[
  'a',
  'bunch',
  'of',
];
```

## Using custom filtering

Sometimes you'd rather write your own custom filer. In PHP, you'd use `array_filter()`, and provide your own custom callback. This module also provides a process plugin:

```
  field_of_array_values:
    plugin: array_filter
    source: some_array_field
    callable: 'my_function'
```

The `callable` parameter works just like it does in core's `callback` process plugin. You can specify a static class function like so:

```
  field_of_array_values:
    plugin: array_filter
    source: some_array_field
    callable:
      - '\Drupal\my_module\MyClass'
      - myMethod
```
