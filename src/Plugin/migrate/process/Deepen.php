<?php

namespace Drupal\migrate_process_array\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Wraps each item in an array with an array.
 *
 * Deepen is the opposite of flatten. After processing a field, particularly
 * when merging fields together, you may get something like the following:
 *
 * @code
 * array(
 *   0 => '123',
 *   1 => '456',
 *   2 => '789',
 * );
 * @endcode
 *
 * Each value is intended as a single field value in a multi-value field, rather
 * than as partials of one field. What we need is to reformat the array as such:
 *
 * @code
 * array(
 *   0 => array(
 *     0 => '123',
 *   ),
 *   1 => array(
 *     0 => '456',
 *   ),
 *   2 => array(
 *     0 => '789',
 *   ),
 * );
 * @endcode
 *
 * Sometimes we need to set the key for each "deepened" value. We can do so with
 * the 'key' item:
 *
 * @code
 * my_field:
 *   source: my_source
 *   plugin: deepen
 *   key: 'target_id'
 * @endcode
 *
 * This would result in output:
 *
 * @code
 * array(
 *   0 => array(
 *     'target_id' => '123',
 *   ),
 *   1 => array(
 *     'target_id' => '456',
 *   ),
 *   2 => array(
 *     'target_id' => '789',
 *   ),
 * );
 * @endcode
 *
 * Then we can do run the result through sub_process:
 *
 * @code
 * field_media:
 *   -
 *     source: the_source
 *     plugin: deepen
 *     key: 'target_id'
 *   -
 *     plugin: sub_process
 *     process:
 *       target_id: target_id
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "deepen"
 * )
 */
class Deepen extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Only process non-empty values.
    if (empty($value)) {
      return NULL;
    }

    // The input must be an array.
    if (!is_array($value)) {
      $value = [$value];
    }

    $out = [];
    foreach ($value as $item) {
      if (empty($this->configuration['key'])) {
        $out[] = [$item];
      }
      else {
        $out[] = [$this->configuration['key'] => $item];
      }
    }

    return $out;
  }
}
