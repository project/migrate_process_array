<?php

namespace Drupal\migrate_process_array\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Enables use of array_filter within a migration.
 *
 * @MigrateProcessPlugin(
 *   id = "array_filter"
 * )
 */
class ArrayFilter extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Only process non-empty values.
    if (empty($value)) {
      return NULL;
    }

    // The input must be an array.
    if (!is_array($value)) {
      $value = [$value];
    }

    // Return using a custom callable, if provided.
    if (!empty($this->configuration['callable'])) {
      return array_filter($value, $this->configuration['callable']);
    }

    // Otherwise, just use the default filter.
    return array_filter($value);
  }
}
