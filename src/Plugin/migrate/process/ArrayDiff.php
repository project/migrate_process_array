<?php

namespace Drupal\migrate_process_array\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Enables use of array_diff within a migration.
 *
 * @MigrateProcessPlugin(
 *   id = "array_diff"
 * )
 */
class ArrayDiff extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Only process non-empty values.
    if (empty($value)) {
      return NULL;
    }

    // The input must be an array.
    if (!is_array($value)) {
      $value = [$value];
    }

    // As well as the array to match against.
    $exclude = $this->configuration['exclude'];
    if (!is_array($exclude)) {
      $exclude = [$exclude];
    }

    // Get the method and callable, if any.
    $method = empty($this->configuration['method']) ? '' : $this->configuration['method'];
    $callable = empty($this->configuration['callable']) ? NULL : $this->configuration['callable'];

    // Return results by method.
    $out = [];
    if ($method == 'assoc') {
      $out = array_diff_assoc($value, $exclude);
    }
    elseif ($method == 'key') {
      $out = array_diff_key($value, $exclude);
    }
    elseif ($method == 'uassoc' && !empty($callable)) {
      $out = array_diff_uassoc($value, $exclude, $callable);
    }
    elseif ($method == 'ukey') {
      $out = array_diff_ukey($value, $exclude, $callable);
    }
    else {
      $out = array_diff($value, $exclude);
    }

    // Migrate treats NULL as empty not not empty arrays.
    if (empty($out)) {
      return NULL;
    }

    return $out;
  }
}
