<?php

namespace Drupal\migrate_process_array\Plugin\migrate\process;

use Drupal\migrate\Plugin\migrate\process\Extract;

/**
 * Same as core's 'extract' plugin, but works within each field value.
 *
 * Normally, a field value is like this:
 *
 * @code
 * array(
 *    0 => array(
 *      'my_key' => 'The value we want',
 *    ),
 *    1 => array(
 *      'my_key' => 'Another value we want',
 *    ),
 * )
 * @endcode
 *
 * With core's extract, we have can only get the first as we have to specify
 * the field delta key.
 *
 * @code
 * my_field:
 *   source: some_field
 *   plugin: extract
 *   index:
 *     - 0
 *     - my_key
 * @endcode
 *
 * With this plugin, we can get the value within each field:
 *
 * @code
 * my_field:
 *   source: some_field
 *   plugin: extract_single
 *   index:
 *     - my_key
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "extract_single",
 *   handle_mulitples = FALSE
 * )
 */
class ExtractSingle extends Extract {
  // There's no need for code changes, since we only want to enforce
  // handling single values.
}
