<?php

namespace Drupal\migrate_process_array\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Enables use of array_intersect within a migration.
 *
 * @MigrateProcessPlugin(
 *   id = "array_intersect"
 * )
 */
class ArrayIntersect extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Only process non-empty values.
    if (empty($value)) {
      return NULL;
    }

    // The input must be an array.
    if (!is_array($value)) {
      $value = [$value];
    }

    // As well as the array to match against.
    $match = $this->configuration['match'];
    if (!is_array($match)) {
      $match = [$match];
    }
    
    // Get the method and callable, if any.
    $method = empty($this->configuration['method']) ? '' : $this->configuration['method'];
    $callable = empty($this->configuration['callable']) ? NULL : $this->configuration['callable'];
    
    // Return results by method.
    $out = [];
    if ($method == 'assoc') {
      $out = array_intersect_assoc($value, $match);
    }
    elseif ($method == 'key') {
      $out = array_intersect_key($value, $match);
    }
    elseif ($method == 'uassoc' && !empty($callable)) {
      $out = array_intersect_uassoc($value, $match, $callable);
    }
    elseif ($method == 'ukey') {
      $out = array_intersect_ukey($value, $match, $callable);
    }
    else {
      $out = array_intersect($value, $match);
    }

    // Migrate treats NULL as empty not not empty arrays.
    if (empty($out)) {
      return NULL;
    }

    return $out;
  }
}
